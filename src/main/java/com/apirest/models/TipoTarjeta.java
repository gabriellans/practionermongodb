package com.apirest.models;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class TipoTarjeta {
    @JsonIgnore
    private String id;
    private String descripcion;

    public TipoTarjeta(){}

    public TipoTarjeta(String id, String descripcion){
        this.id = id;
        this.descripcion = descripcion;
    }

    public String getID(){
        return this.id;
    }

    public String getDescripcion(){
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
