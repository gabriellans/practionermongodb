package com.apirest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Cliente {

    @JsonIgnore
    private String id;
    private String nombre;

    public Cliente(){}

    public Cliente(String id, String nombre){
        this.id = id;
        this.nombre = nombre;
    }

    public String getId(){
        return this.id;
    }

    public String getNombre(){
        return this.nombre;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
