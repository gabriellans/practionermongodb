package com.apirest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.sql.Timestamp;

public class Tarjeta {
    @JsonIgnore
    @Id private String id;
    private String descripcion;
    private long numero;
    private Timestamp fechaCaducidad;
    private Cliente cliente;
    private TipoTarjeta tipoTarjeta;

    public Tarjeta(){}

    public Tarjeta(String id, String descripcion, long numero, Cliente cliente, TipoTarjeta tipoTarjeta){
        this.id = id;
        this.descripcion = descripcion;
        this.numero = numero;
        /*this.fechaCaducidad = fechaCaducidad;*/
        this.cliente = cliente;
        this.tipoTarjeta = tipoTarjeta;
    }

    public String getId(){
        return this.id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getDescripcion(){
        return this.descripcion;
    }

    public long getNumero(){
        return this.numero;
    }

    /*public  Timestamp getFechaCaducidad(){
        return this.fechaCaducidad;
    }*/

    public Cliente getCliente(){
        return this.cliente;
    }

    public TipoTarjeta getTipoTarjeta(){
        return this.tipoTarjeta;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /*public void setFechaCaducidad(Timestamp fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }
*/
    public void setNumero(long numero) {
        this.numero = numero;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setTipoTarjeta(TipoTarjeta tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }
}
