package com.apirest.services.impl;

import com.apirest.models.Tarjeta;
import com.apirest.services.TarjetaService;
import com.apirest.services.repos.RepositorioTarjetas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TarjetaServiceImpl implements TarjetaService {

    @Autowired
    RepositorioTarjetas repositorioTarjetas;

    @Override
    public List<Tarjeta> getTarjetas() {
        return this.repositorioTarjetas.findAll();
    }

    @Override
    public Tarjeta getTarjeta(String id) {
        final Optional<Tarjeta> tr = this.repositorioTarjetas.findById(id);
        return tr.isPresent() ? tr.get() : null;
    }

    @Override
    public Tarjeta addTarjeta(Tarjeta newTarjeta) {
        this.repositorioTarjetas.insert(newTarjeta);
        return newTarjeta;
    }

    @Override
    public Tarjeta updateTarjeta(String id, Tarjeta newTarjeta) {
        newTarjeta.setId(id);
        return this.repositorioTarjetas.save(newTarjeta);
    }

    @Override
    public void removeTarjeta(String id) {
        this.repositorioTarjetas.deleteById(id);
    }
}
