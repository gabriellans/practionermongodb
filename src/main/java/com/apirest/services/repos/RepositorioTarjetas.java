package com.apirest.services.repos;

import com.apirest.models.Tarjeta;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioTarjetas extends MongoRepository<Tarjeta,String> {
}
