package com.apirest.services;

import com.apirest.models.Tarjeta;

import java.util.List;

public interface TarjetaService {

    public List<Tarjeta> getTarjetas();
    public Tarjeta getTarjeta(String id);
    public Tarjeta addTarjeta(Tarjeta newTarjeta);
    public Tarjeta updateTarjeta(String id, Tarjeta newTarjeta);
    public void removeTarjeta(String id);
}
